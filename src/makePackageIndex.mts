import { writeFileSync, existsSync } from "node:fs";
import { listFilesByExtensionSync } from "./listFilesByExtension.mjs";


export const getExportsList = (
	source: string,
	indexFileName = "main.mts"
) => {
	const origin = process.cwd()
	process.chdir(source);
	const contents = listFilesByExtensionSync("./", ["mts", "ts"], 1)
		.filter(
			({ name, basename }) =>
				name !== indexFileName && basename.split(".")[1] != "d"
		)
		.map(({ basename }) => `export * from './${basename}.mjs'`)
		.join("\n");
	//console.log(contents);
	process.chdir(origin);
	writeFileSync(indexFileName, contents, { encoding: "utf-8" });
};

const help = `
USAGE: make-index [root dir] [destination file]

example:
make-index ./src ./src/main.mts
`

export const makePackageIndexCli = () => {
	const path = process.argv[2] ?? "";
	const filename = process.argv[3] ?? ""

	if(/^(-h|--help|help)/.test(path)){
		console.log(help)
		process.exit(0)
	}
	if(path === ''){
		console.log('You need to provide a path')
		console.log(help)
		process.exit(1)
	}
	if(filename === ''){
		console.log('You need to provide a destination filename')
		console.log(help)
		process.exit(1)
	}
	if(!existsSync(path)){
		console.log(`it seems the path ${path} does not exist`)
		console.log(help)
		process.exit(1)
	}
	getExportsList(path, filename);
};