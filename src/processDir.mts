import { readdir } from "node:fs/promises";
import { Dirent, readdirSync } from "node:fs";
import { join, basename, extname } from "node:path";
import { BREAK, SKIP } from "@kinda-ok/convenient/dist/symbols.mjs";

export interface FileDescription {
	name: string
	extension: string,
	basename: string,
	isDirectory: boolean
	path: string
	parent: string
}

interface processDirFunctorParameter {
	BREAK: BREAK;
	SKIP: SKIP;
}

type SyncFunctor<R = unknown[]> = (
	accumulator: R,
	file: FileDescription,
	opts: processDirFunctorParameter
) => BREAK | SKIP | R;

type AsyncFunctor<R = unknown[]> = (
	accumulator: R,
	file: FileDescription,
	opts: processDirFunctorParameter
) => Promise<ReturnType<SyncFunctor<R>>> | ReturnType<SyncFunctor<R>>;

const opts = { BREAK, SKIP } as const;

const entryToFileDescription = (root: string, entry: Dirent) => {
	const extension = extname(entry.name);
	return {
		name: entry.name,
		extension: extension.replace(/^\./, ""),
		basename: basename(entry.name, extension),
		isDirectory: entry.isDirectory(),
		path: join(entry.path, entry.name),
		parent: entry.path,
		root,
	};
};

/**
 * Goes through all files of a directory.
 *
 * return SKIP to skip a directory. Return BREAK to return early
 */
export const processDir = async <R = unknown[],>(
	src: string,
	functor: AsyncFunctor<R>,
	accumulator: R = [] as any,
	maxRecursion: number = 100
): Promise<R> => {
	if (maxRecursion <= 0) {
		return accumulator
	}
	const files = await readdir(src, { withFileTypes: true });

	for (const entry of files) {
		const file = entryToFileDescription(src, entry);
		if (file.isDirectory) {
			const response = await Promise.resolve(functor(accumulator, file, opts));
			if (response !== SKIP) {
				await processDir(
					file.path,
					functor,
					accumulator,
					maxRecursion - 1
				);
			} else if (response === BREAK) {
				return accumulator;
			}
		}else{
			const response = await Promise.resolve(functor(accumulator, file, opts));
			if (response === BREAK) {
				return accumulator;
			}
		}
	}
	return accumulator;
};

/**
 * Goes through all files of a directory.
 *
 * return SKIP to skip a directory. Return BREAK to return early
 */
export const processDirSync = <R = unknown[],>(
	src: string,
	functor: SyncFunctor<R>,
	accumulator: R = [] as any,
	maxRecursion: number = 100
): R => {
	if (maxRecursion <= 0) {
		return accumulator
	}
	const files = readdirSync(src, { withFileTypes: true });


	for (const entry of files) {
		const file = entryToFileDescription(src, entry);
		if (file.isDirectory && maxRecursion > 0) {
			const res = functor(accumulator, file, opts);
			if (res !== SKIP) {
				processDirSync(
					file.path,
					functor,
					accumulator,
					maxRecursion - 1
				);
			}
			if (res === BREAK) {
				return accumulator;
			}
		}else{
			const response = functor(accumulator, file, opts);
			if (response === BREAK) {
				return accumulator;
			}
		}
	}
	return accumulator;
};
