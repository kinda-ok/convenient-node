//@ts-check
import {join} from 'node:path'

/**
 * Returns the the current working directory, or optionally, a resolved path
 * relative to the current working directory.
 * This _is_ relative to where the script is executed.
 * @param rel relative path(s) to join to the location (optional)
 *
 */
export const getDirRelativeToCWD = 
  (...rel: string[]) =>
  {
  ; const dir = process.cwd()
  ; if (rel.length > 0) 
    { return join(dir, ...rel)
    }
  ; return dir
  }
