#!/usr/bin/env node
import {
	parseArgs,
	string,
	bool,
} from "@kinda-ok/convenient";
import { red } from "./cliColors.mjs";
import { replaceInFileSync } from "./processFile.mjs";
import { existsSync } from "fs";

const makeUsage = (file: string) => `
  USAGE:
  ${file} [options] PATH
  don't forget to double escape slashes!
`;

const help = (file: string, options: string[]) => {
	console.log(`
  Processes a file in place
  
${makeUsage(file)}

  options:
    ${options.join("\n    ")},

  example:
    ${file} --search='/(\d+)/' --replace='hello $1' ./file.txt
`);
};

const error = (file: string, message: string) => {
	console.log("");
	console.error(red`  ERROR: ${message}`);
	console.error(makeUsage(file));
	console.error(`  use --help or -h for more explanations`);
	process.exit(1);
};


export const processFileCli = () => {
	const {
		optionsDocs,
		options,
		file,
		rest: [workingPath],
		unknown,
	} = parseArgs(process.argv, {
		search: ["s", "Search regex", string()],
		replace: ["r", "Replacements", string()],
		dryRun: ["n", "Dry run", bool()],
		help: ["h", "this text", bool()],
	});

	if (options.help) {
		help(file, optionsDocs);
		process.exit(0);
	}

	const { search, replace, dryRun } = options

	if(!search || !replace || !workingPath){
		error(file, `you need search, replace, and working path`)
	}
	if(!existsSync(workingPath)){
		error(file, `Path "${workingPath}" was not found or not accessible`)
	}

	
	const result = replaceInFileSync(workingPath, search, replace, dryRun)

	if(dryRun){
		console.log({ search, replace, dryRun, result })
	}
}

processFileCli()