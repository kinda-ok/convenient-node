import { access, constants } from "node:fs/promises";


export const exists = (path:string, mode = constants.R_OK) => access(path, mode).then(()=>true).catch(e=>false)