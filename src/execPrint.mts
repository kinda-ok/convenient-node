import { exec } from "./exec.mjs";

const defaultLog = (...args: any[]) => console.log(...args);
const defaultError = (...args: any[]) => console.error(...args);

export interface ExecOptions {
	log: typeof defaultLog;
	error: typeof defaultError;
	filter: (value: string) => boolean;
}

const defaultFilter = (stderr: string) => true;

const defaultExecOptions = {
	filter: defaultFilter,
	log: defaultLog,
	error: defaultError,
};

export const execPrint = async (
	command: string,
	{
		filter = defaultFilter,
		log = defaultLog,
		error = defaultError,
	}: ExecOptions = defaultExecOptions
) => {
	log(command);
	const { stderr, stdout } = await exec(command);
	if (stderr) {
		if (filter) {
			filter(stderr) && error(stderr);
		} else {
			error(stderr);
		}
	}
	if (stdout) {
		log(stdout);
	}
};
