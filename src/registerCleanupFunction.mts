
const _cleanFunctions: ((...args: any) => any)[] = []

const defaultLog = (...args: any[]) => {}
const defaultError = (...args: any[]) => {}

type SIGNALS = 
| SIGINT
| SIGHUP
| SIGQUIT
| SIGTERM
| SIGUSR1
| SIGUSR2

export type SIGINT = 'SIGINT'
export type SIGHUP = 'SIGHUP'
export type SIGQUIT = 'SIGQUIT'
export type SIGTERM = 'SIGTERM'
export type SIGUSR1 = 'SIGUSR1'
export type SIGUSR2 = 'SIGUSR2'

export interface RegisterCleanupOptions{
	log: typeof defaultLog
	error: typeof defaultError
	/** Optional, for logs */
	name?: string
}


export const registerCleanupFunction = (fn: (...args: any) => any, { log = defaultLog, error = defaultError, name = '[function]'}: Partial<RegisterCleanupOptions> = {}) => {
  _register()
  let hasRun = false
  const wrapper = () => {
    if (hasRun) {
      error(`Already Called: cleanup function for [${name}]`)
      return
    }
    hasRun = true
    removeCleanupFunction(fn)
    log(`running cleanup function for [${name}]`)
    fn()
    log(`success running cleanup function for [${name}]`)
  }
  _cleanFunctions.push(wrapper)
  return wrapper
}


export const removeCleanupFunction = (fn: (...args: any) => any) =>
  _cleanFunctions.indexOf(fn) !== -1 &&
  _cleanFunctions.splice(_cleanFunctions.indexOf(fn), 1)

let _was_registered = false

const _register = 
  () =>
  {
  ; if(_was_registered)
    {
    ; return
    }
  ; _was_registered = true
  ; const exitHandler = 
    () => 
    {
    ; console.log('Exit Handler')
    ; _cleanFunctions.forEach(fn => fn())
    ; _cleanFunctions.length = 0
    }
  ; const onException =
      /**
       * @param {Error} e
       */
      (e: Error) => 
      {
      ; process.stderr.write(e.stack + '\n')
      ; exitHandler()
      ; process.exit(1)
      }
    
  ; const onUnhandledRejection = 
      /**
       * @param {Error} e
       */
      (e: Error) => 
      {
      ; process.stderr.write(e.stack + '\n')
      ; exitHandler()
      }
  ; const onSignal = 
      (_signal: SIGINT | SIGHUP | SIGQUIT | SIGTERM | SIGUSR1 | SIGUSR2) => 
      {
      ; exitHandler()
      ; console.log('Killing process')
      ; process.exit(1)
      }
  ;(['SIGINT'
    , 'SIGHUP'
    , 'SIGQUIT'
    , 'SIGTERM'
    , 'SIGUSR1'
    , 'SIGUSR2'
    ] as SIGNALS[]).forEach(
      ( signal ) => 
      process.on(signal, onSignal.bind(null, signal))
    )

  ; process.on('uncaughtException', onException)
  ; process.on('unhandledRejection', onUnhandledRejection)
  //; process.stdin.resume()
  //; process.on('exit', exitHandler)
  }