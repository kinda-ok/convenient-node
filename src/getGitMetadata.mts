import { join } from "node:path";
import { which, whichSync } from "./which.mjs";
import { execOrSilent, execOrSilentSync } from "./execOrSilent.mjs";
import { CommandNotFoundError } from "./CommandNotFoundError.mjs";
import { readFileIfExists, readFileIfExistsSync } from "./readFileIfExists.mjs";
export interface GitMetaData {
	remote: string;
	commit: string;
	commitShort: string;
	branch: string;
	version: string;
	lastMessage: string;
	submodules: { title: string; path: string }[];
}

export const GIT_COMMANDS = {
	GET_ROOT: `git rev-parse --show-toplevel`,
	GET_REMOTE: `git config --get remote.origin.url`,
	GET_LAST_COMMIT: `git rev-parse HEAD`,
	GET_LAST_COMMIT_SHORT: `git rev-parse --short HEAD`,
	GET_BRANCH: `git branch --show-current`,
	GET_LAST_MESSAGE: `git log -1 --pretty=%B`,
	GET_LAST_VERSION: `git describe --tags --abbrev=0 --match "*.*.*"`,
};

export const submoduleStringToList = (gitSubmodulesFileContent: string) =>
	gitSubmodulesFileContent && typeof gitSubmodulesFileContent === "string"
		? [
				...gitSubmodulesFileContent.matchAll(
					/\[submodule "(.*?)"\]\n\s*path\s=\s(.*?)\n/g
				),
		  ].map(([, title, path]) => ({ title, path }))
		: [];

export const gitRemoteToHttp = (gitRemote: string) =>
	gitRemote
		.replace(/(?:^git@)(?<domain>.*?):(?<path>.+?)$/, "https://$1/$2")
		.replace(/\.git$/, "");

/**
 * Collects git metadata such as commit hash, branch name, etc.
 */
export const getGitMetadata = async (): Promise<GitMetaData> => {
	try {
		await which("git");
	} catch (err: unknown) {
		throw new CommandNotFoundError(`git is not available on this system`);
	}
	const gitRootDir = await execOrSilent(GIT_COMMANDS.GET_ROOT);
	const submodules = submoduleStringToList(await readFileIfExists(join(gitRootDir, ".gitmodules"), "utf-8"))
	const remote = gitRemoteToHttp(await execOrSilent(GIT_COMMANDS.GET_REMOTE));
	const commit = await execOrSilent(GIT_COMMANDS.GET_LAST_COMMIT);
	const commitShort = await execOrSilent(GIT_COMMANDS.GET_LAST_COMMIT_SHORT);
	const branch = await execOrSilent(GIT_COMMANDS.GET_BRANCH);
	const lastMessage = await execOrSilent(GIT_COMMANDS.GET_LAST_MESSAGE);
	const version = await execOrSilent(GIT_COMMANDS.GET_LAST_VERSION);
	return {
		remote,
		commit,
		commitShort,
		branch,
		lastMessage,
		version,
		submodules,
	};
};

/**
 * Collects git metadata such as commit hash, branch name, etc.
 */
export const getGitMetadataSync = (): GitMetaData => {
	try {
		whichSync("git");
	} catch (err: unknown) {
		throw new CommandNotFoundError(`git is not available on this system`);
	}
	const gitRootDir = execOrSilentSync(GIT_COMMANDS.GET_ROOT);
	const submodules = submoduleStringToList(readFileIfExistsSync(join(gitRootDir, ".gitmodules"), "utf-8"))
	const remote = gitRemoteToHttp(execOrSilentSync(GIT_COMMANDS.GET_REMOTE));
	const commit = execOrSilentSync(GIT_COMMANDS.GET_LAST_COMMIT);
	const commitShort = execOrSilentSync(GIT_COMMANDS.GET_LAST_COMMIT_SHORT);
	const branch = execOrSilentSync(GIT_COMMANDS.GET_BRANCH);
	const lastMessage = execOrSilentSync(GIT_COMMANDS.GET_LAST_MESSAGE);
	const version = execOrSilentSync(GIT_COMMANDS.GET_LAST_VERSION);
	return {
		remote,
		commit,
		commitShort,
		branch,
		lastMessage,
		version,
		submodules,
	};
};

