import { getCurrentDate } from "@kinda-ok/convenient";
import {
	getGitMetadata,
	getGitMetadataSync,
	type GitMetaData,
} from "./getGitMetadata.mjs";
import { makeDynamicTemplate } from "@kinda-ok/convenient";

export const getReleaseMetadata = async () => {
	return { ...getCurrentDate(), ...(await getGitMetadata()) };
};

export const getReleaseMetadataSync = () => {
	return { ...getCurrentDate(), ...getGitMetadataSync() };
};

export type ReleaseMetadata = ReturnType<typeof getReleaseMetadataSync>;

export const renderMetaData =
	(template: ReturnType<typeof makeDynamicTemplate>) =>
	(data: ReleaseMetadata) =>
		Object.entries(data)
			.map(([key, value]) => template({ key, value }))
			.join("\n");

export const [renderYamlMetadata, renderIniMetadata] = [
	makeDynamicTemplate(
		`<%=key%>: <%if(key==="submodules"){%> <%='\\n'+value.map(({title,path})=>'  "'+title+'": '+ path).join('\\n')%><%}else{%><%=value%><%}%>`
	),
	makeDynamicTemplate(
		`<%=key%>=<%if(key==="submodules"){%> <%=value.map(({path})=>path).join(',')%><%}else{%><%=value%><%}%>`
	),
].map(renderMetaData);

export const renderJsonMetadata = (data: ReleaseMetadata) =>
	JSON.stringify(data, null, 2);

export const renderMetadataCustom = (
	template: string,
	data: ReleaseMetadata
) => {
	const dynamicTemplate = makeDynamicTemplate(template);
	return renderMetaData(dynamicTemplate)(data);
};

export const renderReleaseHash = ({
	date,
	commitShort,
	version,
	branch,
}: ReleaseMetadata) =>
	[date, commitShort, version, branch].filter(Boolean).join("-");

export const getReleaseHash = () =>
	getReleaseMetadata().then(renderReleaseHash);

export const getReleaseHashSync = () =>
	renderReleaseHash(getReleaseMetadataSync());
