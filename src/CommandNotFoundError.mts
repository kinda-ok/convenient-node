export class CommandNotFoundError extends Error {
	code = 'ENOENT';
	constructor(message: string) {
		super(message);
		this.name = "CommandNotFoundError";
	}
}
