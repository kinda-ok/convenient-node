import { join, delimiter, sep, posix } from "node:path";
import { isWindows } from "./isWindows.mjs";
import { isExecutable, isExecutableSync } from "./isExecutable.mjs";
import { CommandNotFoundError } from "./CommandNotFoundError.mjs";

const rSlash = new RegExp(
	`[${posix.sep}${sep === posix.sep ? "" : sep}]`.replace(/(\\)/g, "\\$1")
);
const rRel = new RegExp(`^\\.${rSlash.source}`);

export interface GetPathInfoOptions {
	/** Use instead of the PATH environment variable. **/
	path: string;
	/** Use instead of the PATHEXT environment variable. **/
	pathExt: string;
	delimiter: string;
	/** Return all matches, instead of just the first one. Note that this means the function returns an array of strings instead of a single string. **/
	all: boolean;
	noThrow: boolean;
}

/**
 * Taken from https://github.com/npm/node-which
 * @param cmd
 * @param options
 */
export const getPathInfo = (
	cmd: string,
	{
		path: optPath = process.env.PATH,
		pathExt: optPathExt = process.env.PATHEXT,
		delimiter: optDelimiter = delimiter,
	}: Partial<GetPathInfoOptions>
) => {
	// If it has a slash, then we don't bother searching the pathenv.
	// just check the file itself, and that's it.
	const pathEnv = cmd.match(rSlash)
		? [""]
		: [
				// windows always checks the cwd first
				...(isWindows ? [process.cwd()] : []),
				...(optPath || "").split(optDelimiter),
		  ];

	if (isWindows) {
		const pathExtExe =
			optPathExt || [".EXE", ".CMD", ".BAT", ".COM"].join(optDelimiter);
		const pathExt = pathExtExe
			.split(optDelimiter)
			.flatMap((item) => [item, item.toLowerCase()]);
		if (cmd.includes(".") && pathExt[0] !== "") {
			pathExt.unshift("");
		}
		return { pathEnv, pathExt, pathExtExe };
	}

	return { pathEnv, pathExt: [""] };
};

const getPathPart = (raw: string, cmd: string) => {
	const pathPart = /^".*"$/.test(raw) ? raw.slice(1, -1) : raw;
	const prefix = !pathPart && rRel.test(cmd) ? cmd.slice(0, 2) : "";
	return prefix + join(pathPart, cmd);
};

interface WhichAsync {
	(
		cmd: string,
		opt: Partial<GetPathInfoOptions> & { all: true } & { noThrow: true }
	): Promise<string[] | null>;
	(cmd: string, opt: Partial<GetPathInfoOptions> & { all: true }): Promise<
		string[]
	>;
	(cmd: string, opt: Partial<GetPathInfoOptions> & { noThrow: true }): Promise<
		string | null
	>;
	(cmd: string): Promise<string>;
}

export const which = (async (
	cmd: string,
	opt: Partial<GetPathInfoOptions> = {}
) => {
	if (cmd == null || cmd === "") {
		if (opt.noThrow) {
			return null;
		}
		throw new SyntaxError(`cmd cannot be null`);
	}
	const { pathEnv, pathExt, pathExtExe } = getPathInfo(cmd, opt);
	const found: string[] = [];

	for (const envPart of pathEnv) {
		const p = getPathPart(envPart, cmd);

		for (const ext of pathExt) {
			const withExt = p + ext;
			const is = await isExecutable(withExt, {
				pathExt: pathExtExe,
				ignoreErrors: true,
			});
			if (is) {
				if (!opt.all) {
					return withExt;
				}
				found.push(withExt);
			}
		}
	}

	if (opt.all && found.length) {
		return found;
	}

	if (opt.noThrow) {
		return null;
	}

	throw new CommandNotFoundError(`${cmd} not found`);
}) as WhichAsync;

interface WhichSync {
	(
		cmd: string,
		opt: Partial<GetPathInfoOptions> & { all: true } & { noThrow: true }
	): string[] | null;
	(cmd: string, opt: Partial<GetPathInfoOptions> & { all: true }): string[];
	(cmd: string, opt: Partial<GetPathInfoOptions> & { noThrow: true }):
		| string
		| null;
	(cmd: string): string;
}

export const whichSync = ((
	cmd: string,
	opt: Partial<GetPathInfoOptions> = {}
) => {
	if (cmd == null || cmd === "") {
		if (opt.noThrow) {
			return null;
		}
		throw new SyntaxError(`cmd cannot be null`);
	}
	const { pathEnv, pathExt, pathExtExe } = getPathInfo(cmd, opt);
	const found: string[] = [];

	for (const pathEnvPart of pathEnv) {
		const p = getPathPart(pathEnvPart, cmd);

		for (const ext of pathExt) {
			const withExt = p + ext;
			const is = isExecutableSync(withExt, {
				pathExt: pathExtExe,
				ignoreErrors: true,
			});
			if (is) {
				if (!opt.all) {
					return withExt;
				}
				found.push(withExt);
			}
		}
	}

	if (opt.all && found.length) {
		return found;
	}

	if (opt.noThrow) {
		return null;
	}

	throw new CommandNotFoundError(`${cmd} not found`);
}) as WhichSync;


export type WhichMultipleOptions = Omit<GetPathInfoOptions, "noThrow" | "all">;

const _whichTestSync = (opts?: WhichMultipleOptions) => (path: string) =>
	(whichSync(path, { ...opts, noThrow: true, all: false }) && true) || false;

const _whichTestAsync =
	(opts?: WhichMultipleOptions) => async (path: string) => {
		if ((await which(path, { ...opts, noThrow: true, all: false })) !== null) {
			return true;
		}
		throw false;
	};


type ExcludesFalse = <T>(x: T | false | undefined) => x is T; 

export const whichTrySync = (paths: (string|undefined)[], opts?: WhichMultipleOptions) =>
	paths.filter(Boolean as any as ExcludesFalse).find(_whichTestSync(opts));


export const whichTry = async (paths: (string|undefined)[], opts?: WhichMultipleOptions) => {
	const predicate = _whichTestAsync(opts)
	try{
		return await Promise.any(paths.filter(Boolean as any as ExcludesFalse).map(path => predicate(path).then(() => path)))
	}catch(e){
		return
	}
}
