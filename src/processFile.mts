import { readFileSync, writeFileSync } from "node:fs";
import { readFile, writeFile } from "node:fs/promises";
import { promisePipe } from "@kinda-ok/convenient/dist/promisePipe.mjs";
import { pipe } from "@kinda-ok/convenient/dist/pipe.mjs";
import { escapeRegExp } from "@kinda-ok/convenient/dist/escapeRegExp.mjs";
import { identity } from "@kinda-ok/convenient";

type Processors =
	| [(input: string) => any, ...((input: any) => any)[], (input: any) => string]

export const processFileSync = (
	path: string,
	processors: Processors,
	dry = false
) => {
	const contents = readFileSync(path, { encoding: "utf-8" });

	if(dry){
		console.log({contents})
	}

	const pipeline = pipe(...processors);
	const result = pipeline(contents);

	if (!dry && result !== contents) {
		writeFileSync(path, result, { encoding: "utf-8" });
	}

	return result;
};

type AsyncProcessors =
	| [
			(input: string) => any,
			...((input: any) => any)[],
			(input: any) => string | Promise<string>
	  ]
	| [(input: string) => string | Promise<string>];

export const processFile = async (
	path: string,
	processors: Processors,
	dry = false
) => {
	const contents = await readFile(path, { encoding: "utf-8" });

	if(dry){
		console.log({contents})
	}

	const pipeline = promisePipe(...processors);
	const result = await pipeline(contents);

	if (!dry && result !== contents) {
		await writeFile(path, result, { encoding: "utf-8" });
	}

	return result;
};

export const regExpFromString = (input: string) => {
	const [, regexpString, flags] =
		input[0] === "/" ? input.split("/") : ["", input, "gi"];
	return new RegExp(regexpString, flags);
};

export const makeTextReplacerFromString = (
	input: string,
	replacements: string,
	debug = false
) => {
	const regexp = regExpFromString(input);
	return (text: string) => {
		if(debug){
			console.log({regexp, replacements})
		}
		return text.replace(regexp, replacements)
	};
};

export const replaceInFileSync = (
	path: string,
	input: string,
	replacements: string,
	dry = false
) => {
	const replacer = makeTextReplacerFromString(input, replacements, dry);
	return processFileSync(path, [replacer, identity], dry);
};

export const replaceInFile = async (
	path: string,
	input: string,
	replacements: string,
	dry = false
) => {
	const replacer = makeTextReplacerFromString(input, replacements);
	return processFile(path, [replacer, identity], dry);
};
