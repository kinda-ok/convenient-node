import { existsSync, readFileSync } from "fs";
import { join } from "path";
import { green, blue } from "./cliColors.mjs";

export type ScriptDescription = string;
export type ScriptCommand = string;
export type ScriptBlockTitle = string;

export interface ScriptsInfo {
	[title: ScriptBlockTitle]: {
		[script: ScriptCommand]: ScriptDescription;
	};
}

export const parsePackageJsonScripts = (search: RegExp) => {
	const file = join(process.cwd(), "package.json");
	const pkg = JSON.parse(
		(existsSync(file) && readFileSync(file, "utf8")) || "{}"
	);
	const scripts = pkg.scripts || {};
	let title = "General";
	return Object.keys(scripts).reduce((scriptsInfo, scriptName) => {
		if (scriptName[0] === "?") {
			const key = scriptName.slice(1);
			if (key[1] === "=") {
				title = key.replace(/^=+\s*(.*?)(?:=)*$/, "$1").trim();
			} else if (key in scripts && key.match(search)) {
				const description = pkg.scripts[scriptName]
					.replace(/^\s*echo\s+/, "")
					.replace(/^(['"])(.*)\1$/, "");
				scriptsInfo[title] = scriptsInfo[title] || {};
				scriptsInfo[title][key] = description;
			}
		}
		return scriptsInfo;
	}, {} as ScriptsInfo);
};

export const scriptsInfoToText = (scriptsInfo: ScriptsInfo) =>
	Object.keys(scriptsInfo)
		.map((title) =>
			[
				blue`${title}`,
				blue`${"-".repeat(title.length)}`,
				...Object.keys(scriptsInfo[title]).map(
					(key) => green`${key}` + `\n` + `  ${scriptsInfo[title][key]}`
				),
			].join("\n")
		)
		.join("\n\n");


export const scriptInfoCli = () => {
	
	const search = new RegExp(
		(process.argv[2] || "").split("").reduce(function (a, b) {
			return a + ".*" + b;
		}, "")
	);

	const scriptsInfo = parsePackageJsonScripts(search)

	const text = scriptsInfoToText(scriptsInfo)

	console.log(`\nAVAILABLE SCRIPTS\n\n` + text + `\n`);
}
