export const reset = "\x1b[0m";

/**
 * Convenience literal
 */
export const tmpl = (strings: TemplateStringsArray, ...substitutions: unknown[]) =>
	strings.reduce(
		(acc, curr, index) => acc + curr + (substitutions[index] || ""),
		""
	);

export const [green, red, blue, yellow] = [
	"\x1b[32m",
	"\x1b[31m",
	"\x1b[34m",
	"\x1b[33m",
].map(
	(color) =>
		(strings: TemplateStringsArray, ...substitutions: unknown[]) =>
			color + tmpl(strings, ...substitutions) + reset
);