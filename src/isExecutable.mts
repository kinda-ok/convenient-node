// taken from https://github.com/isaacs/isexe
import { Stats, statSync } from "node:fs";
import { stat } from "node:fs/promises";
import { isWindows } from "./isWindows.mjs";

export interface IsexeOptions {
	/**
	 * Ignore errors arising from attempting to get file access status
	 * Note that EACCES is always ignored, because that just means
	 * it's not executable. If this is not set, then attempting to check
	 * the executable-ness of a nonexistent file will raise ENOENT, for
	 * example.
	 */
	ignoreErrors?: boolean;

	/**
	 * effective uid when checking executable mode flags on posix
	 * Defaults to process.getuid()
	 */
	uid?: number;

	/**
	 * effective gid when checking executable mode flags on posix
	 * Defaults to process.getgid()
	 */
	gid?: number;

	/**
	 * effective group ID list to use when checking executable mode flags
	 * on posix
	 * Defaults to process.getgroups()
	 */
	groups?: number[];

	/**
	 * The ;-delimited path extension list for win32 implementation.
	 * Defaults to process.env.PATHEXT
	 */
	pathExt?: string;
}

const checkMode = (stat: Stats, options: IsexeOptions) => {
	const myUid = options.uid ?? process.getuid?.();
	const myGroups = options.groups ?? process.getgroups?.() ?? [];
	const myGid = options.gid ?? process.getgid?.() ?? myGroups[0];
	if (myUid === undefined || myGid === undefined) {
		throw new Error("cannot get uid or gid");
	}

	const groups = new Set([myGid, ...myGroups]);

	const mod = stat.mode;
	const uid = stat.uid;
	const gid = stat.gid;

	const u = parseInt("100", 8);
	const g = parseInt("010", 8);
	const o = parseInt("001", 8);
	const ug = u | g;

	return !!(
		mod & o ||
		(mod & g && groups.has(gid)) ||
		(mod & u && uid === myUid) ||
		(mod & ug && myUid === 0)
	);
};

const checkPathExt = (path: string, options: IsexeOptions) => {
	const { pathExt = process.env.PATHEXT || "" } = options;
	const peSplit = pathExt.split(";");
	if (peSplit.indexOf("") !== -1) {
		return true;
	}

	for (let i = 0; i < peSplit.length; i++) {
		const p = peSplit[i].toLowerCase();
		const ext = path.substring(path.length - p.length).toLowerCase();

		if (p && ext === p) {
			return true;
		}
	}
	return false;
};

export const { isExecutableSync, isExecutable } = isWindows
	? (() => {
			/**
			 * Determine whether a path is executable based on the file extension
			 * and PATHEXT environment variable (or specified pathExt option)
			 */
			const isExecutable = async (
				path: string,
				options: IsexeOptions = {}
			): Promise<boolean> => {
				const { ignoreErrors = false } = options;
				try {
					return checkStat(await stat(path), path, options);
				} catch (e) {
					const er = e as NodeJS.ErrnoException;
					if (ignoreErrors || er.code === "EACCES") return false;
					throw er;
				}
			};

			/**
			 * Synchronously determine whether a path is executable based on the file
			 * extension and PATHEXT environment variable (or specified pathExt option)
			 */
			const isExecutableSync = (
				path: string,
				options: IsexeOptions = {}
			): boolean => {
				const { ignoreErrors = false } = options;
				try {
					return checkStat(statSync(path), path, options);
				} catch (e) {
					const er = e as NodeJS.ErrnoException;
					if (ignoreErrors || er.code === "EACCES") return false;
					throw er;
				}
			};
			const checkStat = (stat: Stats, path: string, options: IsexeOptions) =>
				stat.isFile() && checkPathExt(path, options);

			return { isExecutable, isExecutableSync };
	  })()
	: (() => {
			/**
			 * Determine whether a path is executable according to the mode and
			 * current (or specified) user and group IDs.
			 */
			const isExecutable = async (
				path: string,
				options: IsexeOptions = {}
			): Promise<boolean> => {
				const { ignoreErrors = false } = options;
				try {
					return checkStat(await stat(path), options);
				} catch (e) {
					const er = e as NodeJS.ErrnoException;
					if (ignoreErrors || er.code === "EACCES") return false;
					throw er;
				}
			};

			/**
			 * Synchronously determine whether a path is executable according to
			 * the mode and current (or specified) user and group IDs.
			 */
			const isExecutableSync = (
				path: string,
				options: IsexeOptions = {}
			): boolean => {
				const { ignoreErrors = false } = options;
				try {
					return checkStat(statSync(path), options);
				} catch (e) {
					const er = e as NodeJS.ErrnoException;
					if (ignoreErrors || er.code === "EACCES") return false;
					throw er;
				}
			};

			const checkStat = (stat: Stats, options: IsexeOptions) =>
				stat.isFile() && checkMode(stat, options);

			return { isExecutableSync, isExecutable };
	  })();
