import { rename, unlink, cp } from 'node:fs/promises'
import { renameSync, unlinkSync, cpSync } from 'node:fs'


export const moveFile = async (src: string, dest: string) => {

	try{
		rename(src, dest)
	}catch(e){
		await cp(src, dest, {recursive: true})
		await unlink(src);
	}
	
}

export const moveFileSync = (src: string, dest: string) => {

	try{
		renameSync(src, dest)
	}catch(e){
		cpSync(src, dest, {recursive: true})
		unlinkSync(src);
	}
	
}


