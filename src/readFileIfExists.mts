import { existsSync, readFileSync } from "node:fs";
import { exists } from "./exists.mjs";
import { readFile } from "node:fs/promises";

type Options = Parameters<typeof readFile>[1]
type OptionsSync = Parameters<typeof readFileSync>[1]

export async function readFileIfExists(path: string, options?: Options & {encoding: BufferEncoding}, def?: string): Promise<string>
export async function readFileIfExists(path: string, options: BufferEncoding): Promise<string>
export async function readFileIfExists(path: string, options?: Options): Promise<string>
export async function readFileIfExists(path: string, options?: Omit<Exclude<Options, BufferEncoding>,'encoding'>, def?: Buffer): Promise<Buffer>
export async function readFileIfExists(path: string, options: Options = {encoding: 'utf-8'}, def:string|Buffer = ''): Promise<string|Buffer>{
	return await exists(path)
	? await readFile(path, options)
	: def;
}

export function readFileIfExistsSync(path: string, options?: OptionsSync & {encoding: BufferEncoding}, def?: string): string
export function readFileIfExistsSync(path: string, options: BufferEncoding): string
export function readFileIfExistsSync(path: string, options?: OptionsSync): string
export function readFileIfExistsSync(path: string, options?: Omit<Exclude<OptionsSync, BufferEncoding>,'encoding'>, def?: Buffer): Buffer
export function readFileIfExistsSync(path: string, options: OptionsSync = {encoding: 'utf-8'}, def:string|Buffer = ''): string| Buffer{
	return existsSync(path)
	? readFileSync(path, options)
	: def;
}