import {exec, execSync} from './exec.mjs'

/**
 * Runs a shell command, discards any error and returns the output.
 * Always returns a trimmed string.
 */
export const execOrSilent = async (command: string) =>
  (
    await exec(command).catch((/** @type {Error} */ _err: Error) => ({stdout: ''}))
  ).stdout.trim()


/**
 * Runs a shell command, discards any error and returns the output.
 * Always returns a trimmed string.
 */
export const execOrSilentSync = (command: string) => {
  try {
    const stdout = execSync(command, {encoding: 'utf-8'}).trim()
    return stdout
  } catch (e) {}
  return ''
}
