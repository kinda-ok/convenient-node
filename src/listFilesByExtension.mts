import { processDirSync, processDir, type FileDescription } from "./processDir.mjs";
import { escapeRegExp } from "@kinda-ok/convenient/dist/escapeRegExp.mjs";

export const listFilesByExtensionSync = (
	root: string,
	extensions: string[],
	recursion = 1000
) => {
	const regex = new RegExp(extensions.map(escapeRegExp).join("|"),'i');
	
	return processDirSync(
		root,
		(acc, file) => {
			if (file.extension.match(regex) !== null) {
				acc.push(file);
			}
			return acc;
		},
		[] as FileDescription[],
		recursion
	);
};

export const listFilesByExtension = (
	root: string,
	extensions: string[],
	recursion = 1000
) => {
	const regex = new RegExp(extensions.map(escapeRegExp).join("|"));
	extensions.map(escapeRegExp).join("|");
	return processDir(
		root,
		(acc, file) => {
			if (file.extension.match(regex) !== null) {
				acc.push(file);
			}
			return acc;
		},
		[] as FileDescription[],
		recursion
	);
};
