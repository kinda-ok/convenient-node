#!/usr/bin/env node
import { getReleaseMetadataSync, renderReleaseHash } from "./getReleaseMetadata.mjs";
import {
	parseArgs,
	string,
	bool,
} from "@kinda-ok/convenient";
import {
	renderMetadataCustom,
	renderJsonMetadata,
	renderYamlMetadata,
	renderIniMetadata,
} from "./getReleaseMetadata.mjs";
import { red } from "./cliColors.mjs";

const makeUsage = (file: string) => `
  USAGE:
  ${file} [options] [PATH]
`;

const help = (file: string, options: string[]) => {
	console.log(`
  Shows git metadata
  
${makeUsage(file)}
  
  PATH defaults to current

  options:
    ${options.join("\n    ")},

  defaults to JSON
`);
};

const error = (file: string, message: string) => {
	console.log("");
	console.error(red`  ERROR: ${message}`);
	console.error(makeUsage(file));
	console.error(`  use --help or -h for more explanations`);
	process.exit(1);
};

const getReleaseMetadataCli = () => {
	const {
		optionsDocs,
		options,
		file,
		rest: [workingPath],
		unknown,
	} = parseArgs(process.argv, {
		hash: ["hs", "shows the data as single line hash", bool()],
		data: ["da", "shows the data as unquoted JSON (default)", bool()],
		json: ["js", "shows the data as JSON", bool()],
		yaml: ["ya", "shows the data as YAML", bool()],
		ini: ["in", "shows the data as INI", bool()],
		template: ["t", "pass your own line template", string()],
		help: ["h", "this text", bool()],
	});

	if (options.help) {
		help(file, optionsDocs);
		process.exit(0);
	}

	const origPath = process.cwd();
	const path = workingPath ?? origPath;
	const {
		json: asJson,
		yaml: asYaml,
		ini: asIni,
		hash: asHash,
		template,
		data: asData,
	} = options;
	const asTemplate = !!template;

	const switches = [asJson, asYaml, asIni, asTemplate, asData, asHash];

	const amountOfSwitches = switches.filter((s) => s === true).length;
	if (amountOfSwitches > 1) {
		error(file, `You cannot use two switches`);
	}

	process.chdir(path);

	const data = getReleaseMetadataSync()

	process.chdir(origPath);

	const rendered = () => {
		if (asData) {
			return data;
		}
		if(asHash){
			return renderReleaseHash(data)
		}
		if (asJson) {
			return renderJsonMetadata(data);
		}
		if (asYaml) {
			return renderYamlMetadata(data);
		}
		if (asIni) {
			renderIniMetadata(data);
		}
		if (asTemplate) {
			return renderMetadataCustom(template, data);
		}
	};

	console.log(rendered);

	
};

getReleaseMetadataCli();
