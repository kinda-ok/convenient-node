import { execSync } from "node:child_process";
import { exec } from "./exec.mjs";
import { which, whichSync } from "./which.mjs";

export const flatpakExistsSync = (packageName: string) => {
	if(!whichSync(`flatpak`, {noThrow: true})){
		return false
	}
  try {
    execSync(`flatpak list | grep -q "${packageName}"`, {encoding: 'utf8'})
  } 
  catch (error) {
    return false
  }
	return true
};


export const flatpakExists = async (packageName: string) => {
	if(!(await which(`flatpak`, {noThrow: true}))){
		return false
	}
  try {
    await exec(`flatpak list | grep -q "${packageName}"`, {encoding: 'utf8'})
  } 
  catch (error) {
    return false
  }
	return true
};


export const getFlatpackRunCommand = (packageName: string, flags: string[]) => [`flatpak run`, ...flags, packageName].filter(Boolean).join(' ')