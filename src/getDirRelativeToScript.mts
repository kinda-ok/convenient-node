//@ts-check
import {fileURLToPath} from 'node:url'
import {dirname, join} from 'node:path'

/**
 * Returns the this script's location, or optionally, a resolved path relative
 * to the script location.
 * This is _not_ relative to where the script is executed.
 * @param rel relative path(s) to join to the location (optional)
 */
export const getDirRelativeToScript = 
  (...rel: string[]) => 
  {
  ; const filename = fileURLToPath(import.meta.url)
  ; const dir = dirname(filename)
  ; if (rel.length > 0) 
    { return join(dir, ...rel)
    }
  ; return dir
  }
